package jupitermouse.site.project.dataclean

import com.ggstar.util.ip.IpHelper

object IpUtils {
  def getCity(ip:String) = {
    IpHelper.findRegionByIp(ip)
  }

  def main(args: Array[String]) {
    println(getCity("101.231.252.114"))
  }
}
