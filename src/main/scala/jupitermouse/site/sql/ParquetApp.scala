package jupitermouse.site.sql

import org.apache.spark.sql.SparkSession

/**
  * parquet操作
  */
object ParquetApp {

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .appName("ParquestApp")
      .master("local[2]")
      .config("spark.driver.host","localhost").getOrCreate()

    val userDF = spark.read.format("parquest").load("")

    userDF.printSchema()
    userDF.show()

    userDF.select("name","favorite_color").show

    userDF.select("name","favorite_color").write.format("json").save("file:///home/hadoop/tmp/jsonout")

    spark.read.load("file:///home/hadoop/app/spark-2.1.0-bin-2.6.0-cdh5.7.0/examples/src/main/resources/users.parquet").show
    spark.read.format("parquet").option("path","file:///home/hadoop/app/spark-2.1.0-bin-2.6.0-cdh5.7.0/examples/src/main/resources/users.parquet").load().show
    spark.stop()
  }
}
