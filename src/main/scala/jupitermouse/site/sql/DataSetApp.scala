package jupitermouse.site.sql

import org.apache.spark.sql.SparkSession

/**
  * description
  *
  * @author RenQiQiang 2019/07/10 11:57
  */
object DataSetApp {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .master("local{2]")
      .appName("DataFrameCase")
      .config("spark.driver.host","localhost")
      .getOrCreate()

    val df = spark.read.option("header","true").option("inferSchema","true").csv("")
    df.show()

    import spark.implicits._
    val ds = df.as[Sales]
    ds.map(line => line.itemId).show


    spark.sql("seletc name from person").show

    //df.seletc("name")
    df.select("name")

    ds.map(line => line.itemId)

    spark.stop()
  }
  case class Sales(transactionId:Int,customerId:Int,itemId:Int,amountPaid:Double)
}
