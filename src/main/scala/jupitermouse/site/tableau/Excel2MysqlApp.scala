package jupitermouse.site.tableau

import org.apache.spark.sql.SparkSession

object Excel2MysqlApp {

  def main(args: Array[String]): Unit = {
     val spark = SparkSession.builder()
       .appName("Excel2MysqlApp")
       .master("local[2]")
       .getOrCreate()

    val shopDF = spark.read
      .csv("file:///e://workroom/learn/spark/examples/src/main/resources/shop.csv");

    val writeDF = shopDF.select()
    //写入Mysql
    writeDF.write
      .format("jdbc")
      .option("url","jdbc:mysql://localhost:3306/tableau?useUnicode=true&characterEncoding=utf-8&useSSL=false")
      .option("driver", "com.mysql.jdbc.Driver")
      .option("user","root")
      .option("password","root")
      .option("dbtable","test") //操作的表
        .saveAsTable("test")

    spark.stop()

  }
}
